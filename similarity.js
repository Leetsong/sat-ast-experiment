const text = require('./text');

const replacer = (key, value) => {
  if ( /* position information */
    key === 'loc' ||
    key === 'start' || key === 'end' ||
    key === 'line' || key === 'column' ||
    /* in Program node */
    key === 'interpreter' ||
    key === 'directives' ||
    key === 'sourceType') {
    return undefined;
  }

  return value;
};

const stringify = ast => JSON.stringify(ast, replacer);

function similarityByDistanceConstructor(distance) {
  return function (ast1, ast2) {
    const json1 = stringify(ast1);
    const json2 = stringify(ast2);

    const dist = distance(json1, json2);
    const maxDist = Math.max(json1.length, json2.length);
    const sim = (maxDist - dist) / maxDist;

    return sim;
  }
}

function similarityConstructor(similarity) {
  return function (ast1, ast2) {
    const json1 = stringify(ast1);
    const json2 = stringify(ast2);
    return similarity(json1, json2);
  }
}

module.exports = {
  choices: new Proxy(text, {
    set: function (obj, prop, value) {}
  }),

  choose: function(type, method) {
    let constructor = similarityConstructor;
    if (type === 'distance') {
      constructor = similarityByDistanceConstructor;
    }
    return constructor(method);
  }
};
