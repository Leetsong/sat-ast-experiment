const LEVENSHTEIN_DISTANCE = 'LevenshteinDistance';
const JACCARD_DISTANCE = 'JaccardDistance';
const JACCARD_SIMILARITY = 'JaccardSimilarity';

// imported classes
const CLASSES = {
  [LEVENSHTEIN_DISTANCE]: null,
  [JACCARD_DISTANCE]: null,
  [JACCARD_SIMILARITY]: null
};

// java with apache added
let apacheJava = null;

function initializeApacheJava() {
  const fs = require('fs');
  const path = require('path');
  const java = require('java');

  // libs
  const LIB_DIR = path.resolve(__dirname, 'lib');
  const LIBS = fs.readdirSync(LIB_DIR).map(l => path.resolve(LIB_DIR, l));

  // add classpaths
  LIBS.forEach(l => java.classpath.push(l));

  return java;
}

// these classes share the same interface, i.e., 
// 1. a default constructor, and
// 2. an apply() interface
function getFunction(className) {
  if (apacheJava === null) {
    apacheJava = initializeApacheJava();
  }

  return function (s1, s2) {
    if (CLASSES[className] === null) {
      CLASSES[className] = apacheJava.import(
        `org.apache.commons.text.similarity.${className}`);
    }
    const instance = new CLASSES[className]();
    return instance.applySync(s1, s2);
  }
}

module.exports = {
  distance: {
    apache: {
      levenshtein: () => getFunction(LEVENSHTEIN_DISTANCE),
      jaccard: () => getFunction(JACCARD_DISTANCE)
    },
    fastLevenshtein: () => require('fast-levenshtein').get
  },

  similarity: {
    apache: {
      jaccard: () => getFunction(JACCARD_SIMILARITY)
    }
  }
};
