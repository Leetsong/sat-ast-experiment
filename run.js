const {
  choices,
  choose
} = require('./similarity');

const intersection = (setA, setB) => {
  let ret = [];

  for (let elem of setB) {
    if (setA.has(elem)) {
      ret.push(elem);
    }
  }

  return ret;
};

const walkSync = dir => {
  const fs = require('fs');
  const path = require('path');

  const files = fs.readdirSync(dir);
  let fileList = [];

  files.forEach(file => {
    const fp = path.resolve(dir, file);
    if (fs.statSync(fp).isDirectory()) {
      const subFileList = walkSync(fp);
      fileList = [...fileList, ...subFileList];
    } else {
      fileList.push(fp);
    }
  });

  return fileList;
};

function doRun(similarity, folder, sort = (a, b) => a - b) {
  const totalStartTime = Date.now();

  const path = require('path');
  const fs = require('fs');
  const parse = require('./parse');

  // this is for matlab plot
  const matlabLabels = [];
  const matlabX = [];
  const matlabY = [];

  const RELEASE_FOLDER = path.resolve(__dirname, folder);
  const RELEASES = fs.readdirSync(RELEASE_FOLDER).map(
    f => path.resolve(RELEASE_FOLDER, f)
  ).sort(sort);

  let nextFolder = RELEASES[0];
  let nextFileList = walkSync(nextFolder);
  for (let i = 1; i < RELEASES.length; i++) {
    let prevFolder = nextFolder;
    let prevFileList = nextFileList;

    nextFolder = RELEASES[i];
    nextFileList = walkSync(nextFolder);

    let commonList = intersection(
      new Set(prevFileList.map(f => f.slice(prevFolder.length))),
      new Set(nextFileList.map(f => f.slice(nextFolder.length))));
    let prevCommonFileList = commonList.map(f => path.join(prevFolder, f));
    let nextCommonFileList = commonList.map(f => path.join(nextFolder, f));

    console.log(`>> Comparing [${prevFolder.slice(RELEASE_FOLDER.length)}, ${nextFolder.slice(RELEASE_FOLDER.length)}]: common files: ${commonList.length}`);

    let sim = 0;
    let startTime = Date.now();
    for (let i = 0; i < commonList.length; i++) {
      let prevVerFile = prevCommonFileList[i];
      let prevCode = fs.readFileSync(prevVerFile, {
        encoding: 'utf-8'
      });
      let prevAst = parse(prevCode);

      let nextVerFile = nextCommonFileList[i];
      let nextCode = fs.readFileSync(nextVerFile, {
        encoding: 'utf-8'
      });
      let nextAst = parse(nextCode);

      let curStartTime = Date.now();
      let curSim = similarity(prevAst, nextAst);
      let curEndTime = Date.now();
      console.log(`  ${commonList[i]}: sim: ${curSim}, time: ${curEndTime - curStartTime}ms`);

      sim += curSim;
    }
    let endTime = Date.now();
    let totalEndTime = endTime;
    let totalTime = totalEndTime - totalStartTime;
    console.log(`<< [${prevFolder.slice(RELEASE_FOLDER.length)}, ${nextFolder.slice(RELEASE_FOLDER.length)}]: sim: ${sim/=commonList.length}, time: ${endTime - startTime}ms`);
    console.log(`== Analysed ${i} pairs: time: ${totalTime}ms, average-time: ${totalTime / i}ms`);

    // this is for matlab plot
    matlabX.push(i);
    matlabY.push(sim);
    matlabLabels.push(`${prevFolder.slice(RELEASE_FOLDER.length)}:${nextFolder.slice(RELEASE_FOLDER.length)}`);
    console.error('[', matlabX.join(','), ']');
    console.error('[', matlabY.join(','), ']');
    console.error('[', matlabLabels.join(','), ']');
    console.error();
  }
}

// the main logic

module.exports = function run(c, folder, sort) {
  switch (c) {
    case 'dal':
      console.log('CHOOSE distance: Apache Levenshtein');
      doRun(choose('distance', choices.distance.apache.levenshtein()), folder, sort);
      console.log('DONE');
      break;

    case 'daj':
      console.log('CHOOSE distance: Apache Jaccard');
      doRun(choose('distance', choices.distance.apache.jaccard()), folder, sort);
      console.log('DONE');
      break;

    case 'df':
      console.log('CHOOSE distance: Fast Levenshtein');
      doRun(choose('distance', choices.distance.fastLevenshtein()), folder, sort);
      console.log('DONE');
      break;

    case 'saj':
      console.log('CHOOSE similarity: Apache Jaccard');
      doRun(choose('distance', choices.similarity.apache.jaccard()), folder, sort);
      console.log('DONE');
      break;

    default:
      console.log('INVALID CHOICES:', c);
      break;
  }
}
