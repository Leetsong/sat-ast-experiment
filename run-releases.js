require('./run')(process.argv[2], './dataset/express/releases', (a, b) => {
  let arrayA = a.split('/');
  let arrayB = b.split('/');

  let [a1, a2, a3] = arrayA[arrayA.length - 1].split('-')[1].split('.').map(i => parseInt(i));
  let [b1, b2, b3] = arrayB[arrayB.length - 1].split('-')[1].split('.').map(i => parseInt(i));

  if (a1 != b1) {
    return a1 - b1;
  } else if (a2 != b2) {
    return a2 - b2;
  } else {
    return a3 - b3;
  }
});
