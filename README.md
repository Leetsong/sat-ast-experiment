# AST Experiment

> Use [express](https://github.com/expressjs/express.git) as data set

### Prerequisite

1. Node.js 8.x or higher, download it [here](https://nodejs.org/en/download/)
2. Java 8 or higher, download it [here](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
3. Python 2.x, download it [here](https://www.python.org/downloads/)

### Download

``` sh
git clone https://Leetsong@bitbucket.org/Leetsong/sat-ast-experiment.git
npm install
```

### Test and Run

#### Step 1 - Install and Extract Express as Data Set

In the root folder of this project, type:

``` sh
git clone https://github.com/expressjs/express.git
# extract the latest num commits to dataset
./extract-commit <num>
# extract all formal (exclude 0.x.y, alpha, beta, rc) versions to dataset
./extract-tag
```

#### Step 2 - Run and Generate results

In the root folder of this project, type:

``` sh
# to generate results of commits, check your logs in commits-results.txt, and your plot data in commits-matplot.txt
node run-commits.js <option> > commits-results.txt 2> commits-matplot.txt
# to generate results of releases, check your logs in releases-results.txt, and your plot data in releases-matplot.txt
node run-releases.js <option> > releases-results.txt 2> releases-matplot.txt
```

> option:
>
> 1. `dal` - using Levenshtein Distance of Apache Commons Text. **USE IT!!**
>
> 2. `daj` - using Jaccard Distance of Apache Commons Text. **DO NOT USE IT!!**
>
> 3. `df`  - using Levenshtein Distance of Fast Levenshtein. **DO NOT USE IT!!**
>
> 4. `saj` - using Jaccard Similarity of Apache Commons Text. **DO NOT USE IT!!**
