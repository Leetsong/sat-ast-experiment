require('./run')(process.argv[2], './dataset/express/commits', (a, b) => {
  let arrayA = a.split('/');
  let arrayB = b.split('/');

  a = arrayA[arrayA.length - 1].split('-')[1];
  b = arrayB[arrayB.length - 1].split('-')[1];

  return a - b;
});
